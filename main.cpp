#include <SDL2/SDL.h>

#include <cstddef>
#include <cstdint>
#include <iostream>
#include <random>

constexpr std::uint32_t SCREEN_WIDTH = 640;
constexpr std::uint32_t SCREEN_HEIGHT = 480;

/*
 * 1 秒に 60 回更新。
 */
constexpr std::uint32_t FPS = 60;

int main(int argc, char *argv[]) {
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        std::cerr << "SDL init failed." << std::endl;
        return 1;
    }

    SDL_Window *window = SDL_CreateWindow("Hello SDL2", SDL_WINDOWPOS_UNDEFINED,
                                          SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH,
                                          SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    if (!window) {
        std::cerr << "Could not create window." << std::endl;
        SDL_Quit();

        return 1;
    }

    SDL_Renderer *renderer =
        SDL_CreateRenderer(window, -1, SDL_RENDERER_PRESENTVSYNC);
    if (!renderer) {
        std::cerr << "Could not create renderer." << std::endl;
        SDL_DestroyWindow(window);
        SDL_Quit();

        return 1;
    }

    SDL_Texture *screen =
        SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888,
                          SDL_TEXTUREACCESS_TARGET, SCREEN_WIDTH, SCREEN_HEIGHT);

    SDL_Rect rect{0, 0, 100, 50};

    std::random_device seed;
    std::mt19937 engine(seed());

    std::uniform_int_distribution<std::uint8_t> color_for(0, 255);

    std::uniform_int_distribution<std::uint32_t> rect_moving_for(0, 500);

    constexpr std::uint32_t FRAME_PERIOD =
        static_cast<std::uint32_t>(1000.0f / FPS);

    SDL_Color screen_color{0, 0, 0, 255};

    bool should_close = false;
    std::uint32_t last_time = SDL_GetTicks();
    std::uint32_t current_time = 0;
    SDL_Event ev;
    while (!should_close) {
        while (SDL_PollEvent(&ev)) {
            if (ev.type == SDL_QUIT) {
                should_close = true;
            }
        }

        current_time = SDL_GetTicks();

        SDL_SetRenderTarget(renderer, screen);

        SDL_SetRenderDrawColor(renderer, screen_color.r, screen_color.g,
                               screen_color.b, screen_color.a);
        SDL_RenderClear(renderer);

        SDL_SetRenderDrawColor(renderer, 0xff, 0xff, 0xff, 0xff);
        SDL_RenderDrawRect(renderer, &rect);

        SDL_SetRenderDrawColor(renderer, color_for(engine), color_for(engine),
                               color_for(engine), 0xff);
        SDL_RenderFillRect(renderer, &rect);

        SDL_SetRenderTarget(renderer, NULL);
        SDL_RenderCopy(renderer, screen, NULL, NULL);

        SDL_RenderPresent(renderer);

        // デバッグ用 1
        // std::cout << FRAME_PERIOD << "  > ( " << current_time << " - " <<
        // last_time << ") " << std::endl;

        if (FRAME_PERIOD > (current_time - last_time)) {
            // 次のフレーム実行時間まで時間があるなら:

            // 矩形の位置を更新します。
            rect.x = rect_moving_for(engine);
            rect.y = rect_moving_for(engine);

            std::uint32_t wait_time = FRAME_PERIOD - (current_time - last_time);
            // デバッグ用 2
            // std::cout << "wait time: " << wait_time << std::endl;

            // 次のフレーム実行時間まで待ちます。
            SDL_Delay(wait_time);

            last_time = SDL_GetTicks();
        }
    }

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}
